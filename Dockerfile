FROM mambaorg/micromamba:jammy

COPY . $PWD

RUN eval "$(micromamba shell hook --shell bash)" &&\
    micromamba activate base &&\
    micromamba install -y -c conda-forge 'python=3.11' &&\
    python -m pip install .[dev] &&\
    python -m pip cache purge &&\
    python -m pip uninstall -y neuralmag
